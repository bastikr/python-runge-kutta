==================
Python-Runge-Kutta
==================

A module implementing runge kutta methods of various orders. Also usable with
non-standard data types like mpmath multi precision types.
